-- Create a tablecanwith 3 columns: an indexid, a namename, and a
-- capacitycapacity_cl. The types arerespectively: serial, varchar(64), and int.
-- The primary key is the index of the can.

CREATE TABLE can
(
  id SERIAL,
  name varchar(64),
  capacity INT,
  PRIMARY KEY(id)
);

-- Now, create a table student with 4 columns:id,firstname,lastname,male. The
-- types are respectively:serial, varchar(64), varchar(64), boolean and the
-- primary key is the student id.

CREATE TABLE student
(
  id SERIAL,
  firstname VARCHAR(64),
  lastname VARCHAR(64),
  male BOOLEAN,
  PRIMARY KEY(id)
);

-- Alter the table student, to remove the id and replace it by the
-- column login varchar(8) as primary key.

/*  To add an automatically named primary key constraint to a table, noting that
    a table can only ever have one primary key:
    ALTER TABLE distributors ADD PRIMARY KEY (dist_id);
*/

ALTER TABLE student
DROP COLUMN id;

ALTER TABLE student
ADD COLUMN login VARCHAR(8);

ALTER TABLE student
ADD PRIMARY KEY (login);

-- Create a table student_can to store the cans bought by the students. It
-- contains the references to the two other tables.

CREATE TABLE STUDENT_CAN
(
  login VARCHAR(8) REFERENCES student(login),
  id INT REFERENCES can(id)
);
