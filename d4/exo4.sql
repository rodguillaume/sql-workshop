
-- 7.3.1 Request 1
-- Display all the firstnames and names of the students whose firstname starts
-- with a ‘A’.

SELECT firstname, lastname
  FROM student
    WHERE firstname LIKE 'A%';

-- 7.3.2 Request 2
-- Display all the students whose firstname’s length is 5 letters or less.

SELECT firstname, lastname
  FROM student
    WHERE length(firstname) <= 5;

-- 7.3.3 Request 3
-- Display all the students named ‘Paul’. The research must not depend of the
-- case.

SELECT firstname, lastname
  FROM student
    WHERE firstname ILIKE 'paul';
