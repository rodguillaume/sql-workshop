-- 6.4.1 InsertionAdd some cans to yourcantable:•A Red Bull of 33 cl•An Apple
-- Juice of 33 cl
-- Add yourself and 2 persons of your choice in
-- thestudenttable.

INSERT INTO can(name, capacity_cl) VALUES
  ('Red Bull', 33),
  ('Apple Juice', 33);

INSERT INTO student VALUES
  ('guilla_w', 'Rod', 'Guillaume', true),
  ('richar_r', 'Roggan', 'Richard', true),
  ('tony.clonier', 'Tony', 'Clonier', true);



-- 6.4.2 AlterationUpdate the Apple Juice name to Pineapple
-- Juice.

UPDATE can
  SET name = 'Pineapple Juice'
  WHERE name = 'Apple Juice';

-- 6.4.3 DeletionThe Red Bull is not available anymore. Delete this can
-- from your table.

DELETE FROM can
  WHERE name = 'Red Bull';
