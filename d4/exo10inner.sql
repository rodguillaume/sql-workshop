-- Request 1 Display the first name, last name and country of the students
-- living in the United Kingdom.

SELECT firstname, lastname, country
  FROM student
    JOIN address
      ON address_id = address.id
      WHERE country ILIKE 'united Kingdom';

-- Request 2 Display the login of the students, the shop name and the can name
-- to know which student bought which cans and where.

SELECT login, shop.name as shop, can.name as can
  FROM student_can_shop
    JOIN can ON can_id = can.id
      JOIN shop ON shop_id = shop.id
LIMIT 10;

-- Request 3 Display the first names, last names and purchase time of the
-- students who bought some Coke or Diet Coke.
