-- -- 5.7.1 Request 1
-- Display all the can names, ordered in alphabetical order, in
-- -- one column named “can”.10

SELECT name
  FROM can
    ORDER BY name;

-- 5.7.2 Request 2Display all the cans whose capacity
-- -- is greater than 30cl.

SELECT name
  FROM can
    WHERE can.capacity_cl > 30;

-- 5.7.3 Request 3Display the login of the boys in the
-- -- student table in alphabetical order.

SELECT login
  FROM student
    WHERE student.male = true;

-- 5.7.4 Request 4Display the 10 first
-- -- firstnames of the students in alphabetical order.

SELECT firstname
  FROM student
ORDER BY firstname
LIMIT 10;

-- 5.7.5 Request 5Display 10
-- -- student lastnames in alphabetical order, beginning from the third.

SELECT lastname
  FROM student
ORDER BY lastname
OFFSET 3
LIMIT 10;
