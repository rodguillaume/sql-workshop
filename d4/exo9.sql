-- 9.4.1 Request 1 Display all the cans whose capacity is greater than 30cl and
-- smaller than 40cl.

SELECT name
  FROM can
    WHERE capacity_cl BETWEEN 30 and 40;

-- 9.4.2 Request 2 Display all the logins of students without first name.

SELECT substring(login from '(.*)\_') FROM student LIMIT 1;

-- 9.4.3 Request 3 Display all the logins and the purchase_time of the students
-- who bought some cans between the 1st and the 10th of February 2018 in
-- alphabetical order.

SELECT login, purchase_time
  FROM student_can_shop
    WHERE purchase_time::date BETWEEN '2018-02-01'::date AND '2018-02-10'::date;

-- 9.4.4 Request 4 Display all the first names of the students. If a student has
-- no first name, display ‘no_name’ instead.

SELECT COALESCE(firstname, 'no_name') as firstname
  FROM student
LIMIT 10;

-- 9.6.1 Request 5
-- Classify the purchases depending on the date. Before January31th2017, create
-- a label ‘old’. Between the1stof February 2017 and the1stof January 2018 add
-- the label ‘outdated’. Finally, for the dates after the1stofJanuary 2018, add
-- the label ‘current’.

SELECT purchase_time,
  CASE
    WHEN purchase_time::date < '2017-01-31'::date THEN 'old'
    WHEN purchase_time::date < '2018-01-01'::date THEN 'current'
    ELSE 'outdated'
  END as label
    FROM student_can_shop
LIMIT 10;


-- 9.6.2 Request 6
-- Associate a shop with a label of your choice. Display the name of the shop
-- and the label, and order by labels.

SELECT name, encode(name::bytea, 'base64') as label
  FROM shop
ORDER BY label;

SELECT name, substring(name from '^(.)') as label
  FROM shop
ORDER BY label;
