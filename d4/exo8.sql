-- 8.4.1 Request 1 Display all the logins and purchase_time of the students who
-- bought some cans on January 25th, 2017.

SELECT login
  FROM student_can_shop
    WHERE purchase_time::date = to_date('25012017', 'DDMMYYYY');

-- 8.4.2 Request 2 Create and display a timestamp for June 1st, 2017 at 10:56AM.

SELECT '2017-07-01 10:56:00'::timestamp;

-- 8.4.3 Request 3 Display the result of the previous timestamp with the
-- subtraction of the timestamp of May 19th, 2017 at 00:00AM. Call the column
-- ‘interval’.

SELECT '2017-07-01 10:56:00'::timestamp - '2017-05-19 00:00';

-- 8.4.4 Request 4 Display in 3 different columns the day, the month and the
-- year of the purchased cans.

SELECT  extract(day from purchase_time) as day,
        extract(month from purchase_time) as month,
        extract(year from purchase_time) as year
  FROM student_can_shop as Purchases;
