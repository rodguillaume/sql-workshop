-- Create a view called v_student_can to display the cans purchased by the
-- students. It should contain two columns: the student login and the can name.
-- If the student bought three Fanta cans, there should be three entries. Order
-- the results by alphabetical order of the logins first, then of the cans.

CREATE OR REPLACE VIEW v_student_can as
  SELECT login, name
    FROM student_can_shop
      JOIN can ON student_can_shop.can_id = can.id
  ORDER BY login, name;

-- Create a view called v_shop_time that contains every transaction with the
-- shop name and the time it was made at. Sort by shop name and purchase time.

CREATE OR REPLACE VIEW v_shop_time as
  SELECT name, purchase_time
    FROM student_can_shop
      JOIN shop ON shop.id = student_can_shop.shop_id
  ORDER BY name, purchase_time;
