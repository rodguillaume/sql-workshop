-- Write a request that returns all the students that didn’t buy any drinks yet.
-- You are not allowed to use JOINs.

SELECT login
  FROM student
    WHERE login NOT IN (
      SELECT login
        FROM student_can_shop
      GROUP BY login -- GROUP BY optional, but makes request faster.
    )
ORDER BY login;

-- Write a request that displays all the cans the shop 1 (shop_id = 1) is
-- selling. You are not allowed to use JOINs.

SELECT name
  FROM can
    WHERE id IN (
    SELECT can_id
      FROM shop_can
        WHERE shop_id = 1
  )
  ORDER BY name;

-- Write a request that displays the first student that purchased a can.

SELECT login
  FROM student_can_shop
    WHERE purchase_time <= ALL (
      SELECT purchase_time
        FROM student_can_shop
          GROUP BY purchase_time -- still optional, but faster
    );
