-- 7.9.1 Function 1
-- Write the following function:

-- FUNCTION update_can_capacity(can_id INT, new_capacity INT)
--   RETURNS VOID

-- It changes the capacity of a can to the new_capacity. If the can id does not
-- exist, it does nothing.

CREATE OR REPLACE FUNCTION update_can_capacity(can_id INT, new_capacity INT)
  RETURNS VOID AS
$$
BEGIN
  UPDATE can
    SET capacity_cl = new_capacity
      WHERE id = can_id;
END;
$$ LANGUAGE plpgsql;

-- 7.9.2 Function 2
-- Write a function that adds a new shop to the database and returns a boolean
-- that is true on success and false on failure.

CREATE OR REPLACE FUNCTION add_new_shop(name VARCHAR(64))
  RETURNS BOOLEAN AS
$$
BEGIN
  INSERT INTO shop (id, name)
    VALUES (DEFAULT, name);
  RETURN TRUE;
EXCEPTION
  WHEN OTHERS THEN
    RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

-- 7.9.3 Function 3
-- Write a function that gets the list of people who drank more than what is
-- passed as argument to the function.
-- When called like this:
-- SELECT get_thirsty(200);
-- The output of this function will respect the following format:
--    get_thirsty
--    -----------------------------
--    Alexandre Théophile (400cl)
--    Philomène Arnaud (231cl)
--    (2 rows)

/** DRAFT
 * Data we need:
 *  * can (capacity_cl) -> sum of it
 *  * student -> first and last name
 *  * student_can_shop -> list of all cans purchased by each student (LOOP)
 */

CREATE OR REPLACE FUNCTION get_thirsty_shitty_version(amount INT)
  RETURNS SETOF TEXT AS
$$
DECLARE
  login_names RECORD;
  drunk INT;
  login_ varchar(8);
BEGIN
  FOR login_
    IN (
      SELECT distinct login FROM student_can_shop
    )
  LOOP
    SELECT sum(capacity_cl)
      INTO drunk
        FROM student_can_shop
          JOIN can ON can.id = student_can_shop.can_id
            WHERE student_can_shop.login = login_;
    SELECT firstname, lastname
      INTO login_names
        FROM student
          WHERE student.login = login_;
    IF drunk > amount THEN
      RETURN NEXT login_names.firstname || ' ' || login_names.lastname || ' '
                  || '(' || drunk::text || ')';
    END IF;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

/*** CORRECTION ***/

CREATE OR REPLACE FUNCTION get_thirsty(amount INT)
  RETURNS SETOF TEXT AS
$$
DECLARE
  index RECORD;
BEGIN
  FOR index IN
  SELECT firstname, lastname, sum(can.capacity_cl) as total
  FROM student_can_shop
  JOIN student ON student.login = student_can_shop.login
  JOIN can ON can.id = student_can_shop.can_id
  GROUP BY firstname, lastname
  HAVING sum(capacity_cl) > amount
  ORDER BY firstname, lastname
  LOOP
    RETURN NEXT index.firstname || ' ' || index.lastname || ' (' || index.total || ')';
  END LOOP;
  RETURN;
END;
$$ LANGUAGE plpgsql;
