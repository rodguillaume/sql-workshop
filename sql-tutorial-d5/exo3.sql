-- Display the amount of male and female students that are registered in the
-- database.

SELECT male, count(male) FROM STUDENT
  GROUP BY male
  ORDER BY male;

-- We would like to know the average price of a can for each shop. To get two
-- digits after the comma, you can use the round() function. Be careful, the
-- round function expects a numeric value. You will have to CAST the value
-- returned by the average function.

SELECT shop.name, round(CAST(avg(price) AS numeric), 2) as "average price"
  FROM shop
    JOIN shop_can on shop.id = shop_can.shop_id
  GROUP BY shop.id;

-- Display the login and the total amount of money spent on cans by each
-- student. You have to order your results by total. The student who spent the
-- most must be first.

SeLECT login, sum(price) as total
  FROM shop_can
    JOIN student_can_shop ON shop_can.can_id = student_can_shop.can_id
      AND shop_can.shop_id = student_can_shop.shop_id
  GROUP BY login
  ORDER BY total DESC;
